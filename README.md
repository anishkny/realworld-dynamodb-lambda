# ![RealWorld Example App](https://rawgit.com/anishkny/realworld-dynamodb-lambda/master/lambda-node-logo.png)

[![Build Status](https://travis-ci.org/anishkny/realworld-dynamodb-lambda.svg?branch=master)](https://travis-ci.org/anishkny/realworld-dynamodb-lambda)
[![Coverage Status](https://coveralls.io/repos/github/anishkny/realworld-dynamodb-lambda/badge.svg?branch=master)](https://coveralls.io/github/anishkny/realworld-dynamodb-lambda?branch=master)
[![Greenkeeper badge](https://badges.greenkeeper.io/anishkny/realworld-dynamodb-lambda.svg)](https://greenkeeper.io/)
[![Known Vulnerabilities](https://snyk.io/test/github/anishkny/realworld-dynamodb-lambda/badge.svg)](https://snyk.io/test/github/anishkny/realworld-dynamodb-lambda)
[![Gitter](https://img.shields.io/gitter/room/realworld-dev/node-lambda-dynamodb.svg)](https://gitter.im/realworld-dev/node-lambda-dynamodb)

# 🚧 **Work in [progress](https://github.com/anishkny/realworld-dynamodb-lambda/projects/1)!** 🚧

> ### AWS DynamoDB + Lambda codebase containing real world examples (CRUD, auth, advanced patterns, etc) that adheres to the [RealWorld](https://github.com/gothinkster/realworld-example-apps) spec and API.


### [Demo](https://anishkny.github.io/realworld-dynamodb-lambda/test-output/network.html)

This codebase was created to demonstrate a fully fledged fullstack application built with **AWS DynamoDB + Lambda** including CRUD operations, authentication, routing, pagination, and more.

We've gone to great lengths to adhere to the **AWS DynamoDB + Lambda** community styleguides & best practices.

For more information on how to this works with other frontends/backends, head over to the [RealWorld](https://github.com/gothinkster/realworld) repo.

# Getting started

*Requires Node 8 or higher*

## Starting the local server

```
npm install
npm run start
```

This should start local DynamoDB emulator and Serverless offline. You can now make API calls against `http://localhost:3000` like this:

```
curl http://localhost:3000/articles

Serverless: GET /articles (λ: listArticles)
Serverless: The first request might take a few extra seconds
Serverless: [200] {"statusCode":200,"headers":{"Access-Control-Allow-Origin":"*","Access-Control-Allow-Credentials":true},"body":"{\"articles\":[]}"}
```

## Running tests locally
```
npm test
```
See sample test run [log](https://anishkny.github.io/realworld-dynamodb-lambda/test-output/test.log) and [network traffic](https://anishkny.github.io/realworld-dynamodb-lambda/test-output/network.html).

# How it works
